package ejemplo2;

import java.sql.Connection;
import java.sql.SQLException;

public class App {

	public static void main(String[] args) {

		try {
			Connection con = ConnectionFactory.getConexion(ConnectionFactory.MARIADB);
			System.out.println(con);
			System.out.println(con.getMetaData().getDatabaseProductName());

			ConnectionFactory.cerrar();

			con = ConnectionFactory.getConexion(ConnectionFactory.POSTGRESQL);
			System.out.println(con);
			System.out.println(con.getMetaData().getDatabaseProductName());

			ConnectionFactory.cerrar();
			
		} catch (SQLException e) {
			System.out.println("Error estableciendo conexión..." + e.getMessage());
		}

	}

}
