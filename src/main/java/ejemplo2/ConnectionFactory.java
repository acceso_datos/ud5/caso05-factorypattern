package ejemplo2;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

// Patrón Factory. Ejemplo con conexiones.

public class ConnectionFactory {
	final static int MARIADB = 1;
	final static int MYSQL = 2;
	final static int POSTGRESQL = 3;
	private static final String JDBC_URL_MARIADB = "jdbc:mariadb://192.168.56.101:3306/empresa";
	private static final String JDBC_URL_MYSQL = "jdbc:mysql://192.168.56.101:3306/empresa";
	private static final String JDBC_URL_POSTGRESQL = "jdbc:postgresql://192.168.56.101:5432/batoi?schema=empresa";

	private static Connection con = null;

	public static Connection getConexion(int sgbd) throws SQLException {
		String jdbc_url;

		if (con == null) {
			switch (sgbd) {
			case MARIADB:
				jdbc_url = JDBC_URL_MARIADB;
				break;
			case MYSQL:
				jdbc_url = JDBC_URL_MYSQL;
				break;
			case POSTGRESQL:
				jdbc_url = JDBC_URL_POSTGRESQL;
				break;
			default:
				jdbc_url = null;
			}

			Properties pc = new Properties();
			pc.put("user", "batoi");
			pc.put("password", "1234");
			con = DriverManager.getConnection(jdbc_url, pc);
		}
		return con;
	}

	public static void cerrar() throws SQLException {
		if (con != null) {
			con.close();
			con = null;
		}
	}

}
