package ejemplo1;

public class FactoryPatternDemo {

	public static void main(String[] args) {
		ShapeFactory shapeFactory = new ShapeFactory();

		// obtenemos objeto círculo y lo dibujamos
		Shape shape1 = shapeFactory.getShape(ShapeFactory.CIRCLE);
		shape1.draw();

		// obtenemos objeto rectángulo y lo dibujamos
		Shape shape2 = shapeFactory.getShape(ShapeFactory.RECTANGLE);
		shape2.draw();

		// obtenemos objeto cuadrado y lo dibujamos
		Shape shape3 = shapeFactory.getShape(ShapeFactory.SQUARE);
		shape3.draw();

		// obtenemos objeto cuadrado y lo dibujamos
		Shape shape4 = shapeFactory.getShape(ShapeFactory.PENTAGONO);
		shape4.draw();

		// obtenemos objeto triangulo y lo dibujamos
		Shape shape5 = shapeFactory.getShape(ShapeFactory.TRIANGULO);
		shape5.draw();

		// podemos añadir nuevas figuras a la factoria
		// y nuestro código en Demo no se vería afectado lo más mínimo.

	}

}
