package ejemplo1;

//Solución más sencilla e intuitiva (solución procedural usando switch). 
//https://www.oodesign.com/factory-pattern

public class ShapeFactory {
	final static int CIRCLE = 1;
	final static int RECTANGLE = 2;
	final static int SQUARE = 3;
	final static int PENTAGONO = 4;
	final static int TRIANGULO = 5;
	

	// usa getShape para obtener objeto de la figura solicitada
	public Shape getShape(int shapeType) {
		Shape shape = null;
		switch (shapeType) {
		case CIRCLE:
			shape = new Circle();
			break;
		case RECTANGLE:
			shape = new Rectangle();
			break;
		case SQUARE:
			shape = new Square();
			break;
		case PENTAGONO:
			shape = new Pentagono();
			break;
		case TRIANGULO:
			shape = new Triangulo();
			break;
		default:
			shape = null;
		}
		return shape;
	}
}
