package ejemplo3;
import java.util.List;

public interface GenericDAO<T> {
	T find(int id) throws Exception;

	List<T> findAll() throws Exception;

	void insert(T t) throws Exception;

	void update(T t) throws Exception;
	
	void save(T t) throws Exception;

	void delete(T t) throws Exception;

	long size() throws Exception;
}

