package ejemplo3;

import java.sql.SQLException;

// Patrón Factory. Ejemplo con DAOs.

public class DAOFactory {
	
	final static int CLIENTE_DAO = 1;
	final static int VENDEDOR_DAO = 2;
	final static int GRUPO_DAO = 3;
	// ...
	

	// usa getShape para obtener objeto de la figura solicitada
	public GenericDAO<?> getDAO(int DAOType) throws SQLException {
		GenericDAO<?> dao = null;
		switch (DAOType) {
		case CLIENTE_DAO:
			dao = new ClienteDAO();
			break;
		case VENDEDOR_DAO:
			dao = new VendedorDAO();
			break;
		case GRUPO_DAO:
			dao = new GrupoDAO();
			break;
		default:
			dao = null;
		}
		return dao;
	}

}
