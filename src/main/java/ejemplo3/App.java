package ejemplo3;

import ejemplo3.modelo.Cliente;
import ejemplo3.modelo.Grupo;
import ejemplo3.modelo.Vendedor;

public class App {

	public static void main(String[] args) throws Exception {
		DAOFactory daoFactory = new DAOFactory();

		ClienteDAO cliDao = (ClienteDAO) daoFactory.getDAO(DAOFactory.CLIENTE_DAO);
		Cliente cli = cliDao.find(1);
		System.out.println(cli);

		VendedorDAO venDao = (VendedorDAO) daoFactory.getDAO(DAOFactory.VENDEDOR_DAO);
		Vendedor ven = venDao.find(1);
		System.out.println(ven);

		GrupoDAO gruDao = (GrupoDAO) daoFactory.getDAO(DAOFactory.GRUPO_DAO);
		Grupo gru = gruDao.find(1);
		System.out.println(gru);

	}

}
